import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Data } from './../data';
import { Observable } from 'rxjs';
import { getTranslationDeclStmts } from '@angular/compiler/src/render3/view/template';

@Injectable({
  providedIn: 'root'
})
export class DataProviderService {

  constructor(private httpClient: HttpClient) { }

  public getData(): Observable<any> {
    return this.httpClient.get<Array<Data>>('data/cribs.json')
    .pipe(map(r => {
      // const byBathrooms = groupBy('bathrooms');
      const byArea = groupBy('area');
      return byArea(r);
    }));
  }

}
const groupBy = key => array =>
  array.reduce(
    (objectsByKeyValue, obj) => ({
      ...objectsByKeyValue,
      [obj[key]]: (objectsByKeyValue[obj[key]] || []).concat(obj)
    }),
    {}
  );
