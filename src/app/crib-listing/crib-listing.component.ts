import { Component, OnInit } from '@angular/core';
import { DataProviderService } from './../service/data-provider.service';

@Component({
  selector: 'app-crib-listing',
  templateUrl: './crib-listing.component.html',
  styleUrls: ['./crib-listing.component.css']
})
export class CribListingComponent implements OnInit {


  cribs: any;

  constructor(private dataProvider:DataProviderService) { }

  ngOnInit(): void {
   this.dataProvider.getData().subscribe(data => {this.cribs = data});
  }
}
