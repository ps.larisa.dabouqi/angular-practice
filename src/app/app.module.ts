import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
 

import { AppComponent } from './app.component';
import { CribListingComponent } from './crib-listing/crib-listing.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { DataProviderService } from './service/data-provider.service';

@NgModule({
  declarations: [
    AppComponent,
    CribListingComponent,
    PieChartComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    HttpClientModule
  ],
  providers: [
     DataProviderService 
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
